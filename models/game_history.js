'use strict';
const {
  Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Game_History extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Game_History.belongsTo( models.User , { foreignKey: 'user_id' });
    }
  }
  Game_History.init({
    user_id: DataTypes.INTEGER,
    status_winner: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Game_History',
  });
  
  
  return Game_History;
};