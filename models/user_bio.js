'use strict';
const {
  Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class User_Bio extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User_Bio.belongsTo(models.User, { foreignKey: 'user_id' });
    }
  }
  User_Bio.init({
    user_id: DataTypes.INTEGER,
    fullname: DataTypes.STRING,
    age: DataTypes.INTEGER,
    location: DataTypes.STRING,
    phonenumber: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User_Bio',
  });
  

  return User_Bio;
};