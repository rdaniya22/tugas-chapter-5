const usersList = [];
const md5 = require('md5');
const db = require('../models');
const { Op } = require("sequelize");

class UserModel{
  //get all users
  getAllUsers = async () => {

    const dataUser= await db.User.findAll();
    console.log(dataUser);
    return dataUser;

  }

  getAllGame = async (id) => {

    const dataGames= await db.Game_History.findAll({
      where: {
        user_id: id
      }
    });
   
    return dataGames;

  }

  getSingleBio = async (id) => {
    const dataUser = await db.User_Bio.findOne({ where: { user_id: id } });
    return dataUser;
  }
  updateSingleBio = async (id,dataBody) => {
    const dataUser = await db.User_Bio.update(
      { fullname: dataBody.fullname,
        age: dataBody.age,
        location: dataBody.location,
        phonenumber: dataBody.phonenumber },
      { where: { user_id: id } }
    )
    //console.log("data model" + dataBody);
    return dataUser;
  }

  //check user if exist
  isUserRegistered = async (dataBody) =>{
    const existUser = await db.User.findOne({ where :{ [Op.or] : [
      {username: dataBody.username},
      {email: dataBody.email}
    ]
    }
    });

    if(existUser){
      return true
     }else{
      false
     }
    /*const existUser = usersList.find((data)=> {
      return data.username === dataBody.username || data.email === dataBody.email
     });
    
     */
    
  }

  isUserIdExisted = async (dataBody) =>{
    const existUser = await db.User_Bio.findOne({ where: { user_id: dataBody.user_id } });

    if(existUser){
      return true
     }else{
      false
     }
    /*const existUser = usersList.find((data)=> {
      return data.username === dataBody.username || data.email === dataBody.email
     });
    
     */
    
  }

  isUserIdCreated = async (dataBody) =>{
    const existUser = await db.User.findOne({ where: { id: dataBody.user_id } });

    if(existUser){
      return true
     }else{
      false
     }
    /*const existUser = usersList.find((data)=> {
      return data.username === dataBody.username || data.email === dataBody.email
     });
    
     */
    
  }

  //record new data
  recordNewData = (dataBody) => {
    db.User.create({
     
      username: dataBody.username,
      email: dataBody.email,
      password: md5(dataBody.password),
      
    });
    
  }

  //record new history game
  recordNewGame = (dataBody) => {
    db.Game_History.create({
     
      user_id: dataBody.user_id,
      status_winner: dataBody.status_winner,
     
      
    });
    
  }

  //record new history game
  recordNewBio = (dataBody) => {
    db.User_Bio.create({

      user_id: dataBody.user_id,
      fullname: dataBody.fullname,
      age: dataBody.age,
      location: dataBody.location,
      phonenumber: dataBody.phonenumber
     
      
    });
    
  }

  //user login

  userLogin = async (username,password) => {

    const dataUser = await db.User.findOne({where: {username: username, password: md5(password)}})
    /*const dataUser = usersList.find((data) => 
    {
      return (
        data.username === username && data.password === md5(password)
      )

    })*/

    return dataUser;

  }

}

module.exports = new UserModel();