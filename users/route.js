const express = require('express');
const userRouter = express.Router();
const userController = require('./controller');


//get all users
userRouter.get("/", userController.getAllUsers);

//to register user
userRouter.post("/register", userController.registerUsers);

//to login user
userRouter.post("/login", userController.login);

//to add game history
userRouter.post("/history", userController.history);

//to add user bio
userRouter.post("/add-bio", userController.addBio);

//get 1 bio
userRouter.get("/detail/:id",userController.getSingleBio);

//update 1 bio
userRouter.put("/update/:id",userController.updateSingleBio);

//get all games from 1 id
userRouter.get("/game/:id",userController.getAllGame);

module.exports = userRouter;