const userModel = require("./model");
class UserController{
  getAllUsers = async (req,res) => {
    const allUsers = await userModel.getAllUsers();
    return res.json(allUsers);
  }
  
  getAllGame = async (req,res) => {
    const { id } = req.params;
    try{
      const games = await userModel.getAllGame(id);
      return res.json(games);
      
    }catch(error){
      return res.json({message: "user_id does not exist"})
    }
    
  }


  getSingleBio = async (req,res)=> {
    const { id } = req.params;
    try{
      const userBio = await userModel.getSingleBio(id);
      if(userBio){
        return res.json(userBio);
      }else{
        return res.json({message: "user_id does not exist"});
      }
      
      
    }catch(error){
      return res.json({message: "user_id does not exist"})
    }

    
    
   
  }

  updateSingleBio = async (req,res)=> {
    const { id } = req.params;
    const dataBody = req.body;
    
    console.log(dataBody);
    const update = await userModel.updateSingleBio(id,dataBody);
    //return res.json({message: "updated"})
    try{
      const userBio = await userModel.getSingleBio(id);
      console.log("this id databody "+dataBody)
      if(userBio){
        const update = await userModel.updateSingleBio(id,dataBody);
        return res.json({message: "updated successfully"});
      }else{
        return res.json({message: "user_id does not exist"});
      }
      
      
    }catch(error){
      return res.json({message: "user_id does not exist"})
    }

   
    
    
    
  }

  registerUsers = async (req,res) => {
    const dataBody = req.body;
    if(dataBody.username === undefined || dataBody.username.trim() ===''){
      res.statusCode = 400;
      return res.json({message: "username is invalid"})
  
    }
    if(dataBody.email === undefined || dataBody.email.trim() ===''){
      res.statusCode = 400;
      return res.json({message: "email is invalid"})
  
    }
    if(dataBody.password === undefined || dataBody.password.trim() ===''){
      res.statusCode = 400;
      return res.json({message: "password is invalid"})
  
    }
    
  
    //check exist data
    const existData = await userModel.isUserRegistered(dataBody);
    if(existData){
      return res.json({message: "username or email is exist"})
  
    }
    

    //record data to userList
    const recordData = userModel.recordNewData(dataBody);
    
    return res.json({message: "new user is added " })
  };

  //checking body req
  history = async (req,res) => {
    const dataReq = req.body;
    if( dataReq.user_id === undefined || isNaN(dataReq.user_id)){
      res.statusCode = 400;
      return res.json({message: "please fill user_id"})
  
    }
    if(dataReq.status_winner === undefined || dataReq.status_winner .trim() ===''){
      res.statusCode = 400;
      return res.json({message: "please fill winner"})
  
    }
    
    const existData = await userModel.isUserIdCreated(dataReq);
    if(existData){
      const recordGame= await userModel.recordNewGame(dataReq);
  
    }else{
      return res.json({message: "user_id doesnt exist"})
    }

    //record data to userList
   
    
    return res.json({message: "history recorded " })
  };

  addBio = async (req,res) => {
    const dataReq = req.body;
    if( dataReq.user_id === undefined || isNaN(dataReq.user_id)){
      res.statusCode = 400;
      return res.json({message: "please fill user_id"})
  
    }
    if(dataReq.fullname === undefined || dataReq.fullname .trim() ===''){
      res.statusCode = 400;
      return res.json({message: "please fill fullname"})
  
    }
    
    const existData = await userModel.isUserIdExisted(dataReq);
    if(existData){
      return res.json({message: "please use update URL"})
  
    }
    const recordBio=  await userModel.recordNewBio(dataReq);
    
    return res.json({message: "user bio is recorded " })
  };

  login = async (req,res) => {
    const {username, password}= req.body;
    const dataUser = await userModel.userLogin(username,password);
    if(dataUser){
      return res.json(dataUser);
    }else{
      return res.json({message : "invalid credential"})
    }
  };
}

module.exports = new UserController();